---
forceTheme: purple
---
# Medical Handbook
<center>
Select a subdivision
</center>
<div class="row">
  <div class="column">
    <a href="../medic/">
      <center>
      <img src="https://t6.rbxcdn.com/b0bdf6de15beceba3b8a3502214d59ed"
        style="border-radius: 50%;">
      
   <p>Medical Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../hazmat/">
      <center>
      <img src="https://t5.rbxcdn.com/677f0db1b57fedc0502bfeafd774acd6"
        style="border-radius: 50%;">
      
   <p>Hazmat Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../fire/">
      <center>
      <img src="https://t2.rbxcdn.com/a796198d3e727715beb0158d5249bc45"
        style="border-radius: 50%;">
      
   <p>Fire Team</p>
      </center>
    </a>
  </div>
</div>

## Medical Team Rules
::: danger ATTENTION:
AtomicMoosh (Medical Chief) is currently rewriting the entire MT handbook. Furthermore, the CM Division (Combat Medic) has been removed as a official subdivision. This means CM will also be removed from the website handbook!
:::
::: warning NOTE:
This is the handbook for the subsection of the Pinewood Emergency Team
**MEDICAL TEAM**

Please don't forget that is made for the medic team only, so no rules that are stated here apply on the other sub-divisions. 
Only if stated in their respective handbooks. 
:::

### **(MT. 1)** Always have a med kit
Medical Team members are required to carry the PET Medical “lunchbox” medkit at all times. The PB throwable medkit is fine for anybody to use. The MT may not use the Fire Team hose but can use the fire extinguishers found across the map. MT members must also wear the MT uniform at all times. This can be found at the PET HQ.

### **(MT. 2)** Heal everyone (Except enemies)
Medical Team members are required to heal everyone they see who are hurt, PBST members get healing priority. PET is not to heal enemies (ex. TMS, Raiders).

### **(MT. 3)** Combat healing
Medical Team members are permitted to heal while they are in combat, though caution is to be taken in making sure TMS or other raiders are not accidentally healed.

### **(MT. 4)** The Mayhem Syndicate
Medical Team members are allowed to kill The Mayhem Syndicate if said Syndicate members are actively hurting visitors or allies.

### **(MT. 5)** Kill on Sight
During Chaos Raids, The Mayhem Syndicate is “Kill on Sight”. No other time is anybody on KoS unless authorized by an Emergency Medic+.
